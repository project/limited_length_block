$(document).ready(function() {
	$("textarea.limited").each(function() {
		var elClass = $(this).attr('class');	
		var maxWords = elClass.substring((elClass.indexOf('['))+1, elClass.lastIndexOf(']'));
		
		$(this).before('<div class="wordCount"><span class="current-count">0</span> / <span class="max-count">' + maxWords + '</span></div>');
		
		applyCount($(this));
		
		$(this).bind('keyup click blur focus change paste', function() {			
			applyCount($(this));
		});
	});
});

function applyCount(element){
	maxWords = $('#edit-length-limit-limit').val();
	type = $('#edit-length-limit-type').val();

	if (maxWords){
		if (type=='words'){
			var numWords = jQuery.trim(element.val()).split(' ').length;
		}
		else{
			var numWords = jQuery.trim(element.val()).length;
		}
		
		if(element.val() === '') {
			numWords = 0;
		}	
		element.siblings('.wordCount').children('.current-count').text(numWords);
		element.siblings('.wordCount').children('.max-count').text(maxWords);
		
		if(numWords > maxWords && maxWords != 0) {
			element.siblings('.wordCount').addClass('limit-error');
		} else {
			element.siblings('.wordCount').removeClass('limit-error');	
		}
	}
	else{
		element.siblings('.wordCount').removeClass('limit-error');
		element.siblings('.wordCount').text('');
	}
}